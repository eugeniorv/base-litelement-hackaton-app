import { LitElement, html} from 'lit-element';

class CarFichaListado extends LitElement {

    static get properties() {
        return {
            id: {type: String},
            desc: {type: String},
            price: {type: Number},
            color: {type: String},
            foto: {type: String}
        };
    }

    constructor() {
        super();

    }

    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <div class="card h-100">                
                <div class="card-body">
                    <img src="${this.foto}" alt="${this.desc}" height="120" width="200" class="card-img-top"/>
                    <h5 class="card-title"><label>Id: </label> ${this.id}</h5>
                    <h5 class="card-title"><label>Desc: </label> ${this.desc}</h5>
                    <p class="card-text"><label>Color: </label> ${this.color}</p>
                    <h3 class="card-title">
                        <strong>${this.price}&nbsp;€</strong>
                        <button @click="${this.buyCar}" class="btn btn-warning col-5"><strong>Comprar</strong></button>
                    </h3>
                </div>
                <div class="card-footer">
                    <button @click="${this.deleteCar}" class="btn btn-danger col-5"><strong>X</strong></button>
                    <button @click="${this.moreInfo}" class="btn btn-info col-5"><strong>+ Info</strong></button>
                </div>
            </div>
        `;
    }

    buyCar(e) {
        console.log("car-ficha-listado.js buyCar");
        console.log("Se va a comprar el coche id " + e.detail.id);

        this.dispatchEvent(
            new CustomEvent("buy-car", {
                detail: {
                    purchase: {
                        id: this.id,
                        userId: "1",
                        amount: this.price,

                        purchaseItems: {
                            "1": this.id    //CarId - Cantidad
                        }
                    },
                }
            })
        );
    }

    deleteCar(e) {
        console.log("car-ficha-listado.js deleteCar");
        console.log("Se va a borrar el coche id " + this.id);

        this.dispatchEvent(
            new CustomEvent("delete-car", {
                detail: {
                    id: this.id
                }
            })
        );
    }

    moreInfo(e) {
        console.log("car-ficha-listado.js moreInfo");
        console.log("Se pide mas info del coche " + this.id);

        this.dispatchEvent(
            new CustomEvent("info-car", {
                detail: {
                    id: this.id
                }
            })
        );
    }
}

customElements.define('car-ficha-listado', CarFichaListado)