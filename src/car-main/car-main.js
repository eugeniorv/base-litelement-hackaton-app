import { LitElement, html} from 'lit-element';

import '../car-dm/car-dm.js';
import '../car-ficha-listado/car-ficha-listado.js';
import '../car-form/car-form.js';

class CarMain extends LitElement {

    static get properties() {
        return {
            cars : {type: Array},
            showCarForm: {type: Boolean}
        };
    }

    constructor() {
        super();

        this.cars = [];
        this.showCarForm = false;
    }

    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <section class="p-3 mb-2 bg-light text-dark">
            <h2 class="text-center">Listado de Coches</h2>
            <div id="carList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${ this.cars.map(
                    car => html`<car-ficha-listado 
                                        id="${car.id}" 
                                        desc="${car.desc}" 
                                        color="${car.color}" 
                                        foto="${car.foto}" 
                                        price="${car.price}" 
                                        @buy-car="${this.buyCar}" 
                                        @delete-car="${this.deleteCar}" 
                                        @info-car="${this.infoCar}"
                                    >
                                </car-ficha-listado>`
                )}
                </div>
            </div>
            <div class="row">
                <car-form id="carForm" class="d-none border rounded border-primary"
                    @car-form-close="${this.carFormClose}"
                    @car-form-store="${this.carFormStore}">
                </car-form>
            </div>
        </section>
        <car-dm
            @cars-data-updated="${this.carsDataUpdated}">
        </car-dm>
        `;
    }

    updated(changedProperties) {
        console.log("car-main.js - updated(changedProperties)");
        
        if(changedProperties.has("showCarForm")) {
            console.log("car-main.js - Ha cambiado el valor de la propiedad showCarForm");

            if(this.showCarForm === true){
                this.showCarFormData();
            } else {
                this.showCarList();
            }
        }

        if(changedProperties.has("cars")) {
            console.log("car-main.js - He cambiado el valor de la propiedad cars");

            //TO DO Throw event
            this.dispatchEvent(
                new CustomEvent(
                    "updated-cars",
                    {
                        detail : {
                            cars : this.cars
                        }
                    }
                )
            )

        }

    }

    carsDataUpdated(e) {
        console.log("car-main.js - carsDataUpdated");
    
        this.cars = e.detail.cars;
    }

    buyCar(e) {
        console.log("car-main.js - buyCar");
        console.log("car-main.js - Se va a comprar el coche id " + e.detail.purchase.id);
        console.log(JSON.stringify(e.detail.purchase));

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 200 - OK - Petición realizada correctamente");

            }else if (xhr.status === 400){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 400 - BAD REQUEST - Esta respuesta significa que el servidor no pudo interpretar la solicitud dada una sintaxis inválida.");

            }else if (xhr.status === 404){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 404 - NOT FOUND - El servidor no pudo encontrar el contenido solicitado. Este código de respuesta es uno de los más famosos dada su alta ocurrencia en la web.");
            }
        }

        xhr.open("POST","http://localhost:8080/hackaton/purchases/");
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(e.detail.purchase));  
    }

    deleteCar(e) {
        console.log("car-main.js - deleteCar");
        console.log("car-main.js - Se va a borrar el coche id " + e.detail.id);

        // REFACTORIZACIÓN API
        // SI SE COMENTA HAY QUE RECARGAR LA PÁGINA!
        this.cars = this.cars.filter(
                car => car.id != e.detail.id
        );

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 200 - OK - Petición realizada correctamente");

            }else if (xhr.status === 400){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 400 - BAD REQUEST - Esta respuesta significa que el servidor no pudo interpretar la solicitud dada una sintaxis inválida.");

            }else if (xhr.status === 404){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 404 - NOT FOUND - El servidor no pudo encontrar el contenido solicitado. Este código de respuesta es uno de los más famosos dada su alta ocurrencia en la web.");
            }
        }

        xhr.open("DELETE","http://localhost:8080/hackaton/cars/"+e.detail.id);
        xhr.send();
    }

    showCarList() {
        console.log("car-main.js - showCarList");
        console.log("car-main.js - Mostrando el listado de coches");

        this.shadowRoot.getElementById("carList").classList.remove("d-none");
        this.shadowRoot.getElementById("carForm").classList.add("d-none");
    }

    showCarFormData() {
        console.log("car-main.js - showCarFormData");
        console.log("car-main.js - Mostrando formulario de coche");

        this.shadowRoot.getElementById("carForm").classList.remove("d-none");
        this.shadowRoot.getElementById("carList").classList.add("d-none");
    }

    infoCar(e) {
        console.log("car-main.js - infoCar");
        console.log("car-main.js - Se ha pedido más información del coche " + e.detail.id);

        let choosenCar = this.cars.filter(
            car => car.id === e.detail.id
        );
        console.log(this.cars);
        console.log(choosenCar);
        console.log(choosenCar[0].id);
        
        //ERROR RECURRENTE: EVITAR!!
        //console.log(choosenCar.name);

        let car = {};
        car.id = choosenCar[0].id;
        car.desc = choosenCar[0].desc;
        car.color = choosenCar[0].color;
        car.price = choosenCar[0].price;
        car.foto = choosenCar[0].foto;

        //this.shadowRoot.querySelector
        this.shadowRoot.getElementById("carForm").car = car;
        this.shadowRoot.getElementById("carForm").editingCar = true;
        this.showCarForm = true;
    }

    carFormClose() {
        console.log("car-main.js - carFormClose");
        console.log("car-main.js - Se ha cerrado el formulario de coche");

        this.showCarForm = false;
    }

    carFormStore(e) {
        console.log("car-main.js - carFormStore - inicio");
        console.log(JSON.stringify(e.detail.car));
        
        if(e.detail.editingCar === true) {
            console.log("car-main.js - Se va a actualizar el coche id " + e.detail.car.id);
            console.log("car-main.js - UPDATE car "+e.detail.car);
            
            // UPDATE
            this.cars = this.cars.map(
                car => car.id === e.detail.car.id 
                    ? car = e.detail.car : car
            );

            console.log("car-main.js -car "+e.detail.car);

            let xhr = new XMLHttpRequest();

            xhr.onload = () => {
                if (xhr.status === 200){
                    //Ejemplo de async -> este log sale lo último en la carga de la página
                    console.log("STATUS 200 - OK - Petición realizada correctamente");

                }else if (xhr.status === 400){
                    //Ejemplo de async -> este log sale lo último en la carga de la página
                    console.log("STATUS 400 - BAD REQUEST - Esta respuesta significa que el servidor no pudo interpretar la solicitud dada una sintaxis inválida.");

                }else if (xhr.status === 404){
                    //Ejemplo de async -> este log sale lo último en la carga de la página
                    console.log("STATUS 404 - NOT FOUND - El servidor no pudo encontrar el contenido solicitado. Este código de respuesta es uno de los más famosos dada su alta ocurrencia en la web.");

                }else if (xhr.status === 405){
                    //Ejemplo de async -> este log sale lo último en la carga de la página
                    console.log("STATUS 405 - NOT ALLOWED");
                }
            }

            xhr.open("PUT","http://localhost:8080/hackaton/cars/"+e.detail.car.id);
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.send(JSON.stringify(e.detail.car));

        } else {
            console.log("car-main.js - Se va a almacenar el coche "+e.detail.car.id);
            console.log("car-main.js - ADD car "+e.detail.car);

            // ADD
            this.cars = [...this.cars, e.detail.car]; //JS Spread Syntax(Python sequence)
            //this.cars = [this.cars, e.detail.car]; //Spread syntax: array cuyo 1er elemento es el array original

            console.log("car-main.js - this.cars: "+this.cars);

            let xhr = new XMLHttpRequest();

            xhr.onload = () => {
                if (xhr.status === 200){
                    //Ejemplo de async -> este log sale lo último en la carga de la página
                    console.log("STATUS 200 - OK - Petición realizada correctamente");

                }else if (xhr.status === 400){
                    //Ejemplo de async -> este log sale lo último en la carga de la página
                    console.log("STATUS 400 - BAD REQUEST - Esta respuesta significa que el servidor no pudo interpretar la solicitud dada una sintaxis inválida.");

                }else if (xhr.status === 404){
                    //Ejemplo de async -> este log sale lo último en la carga de la página
                    console.log("STATUS 404 - NOT FOUND - El servidor no pudo encontrar el contenido solicitado. Este código de respuesta es uno de los más famosos dada su alta ocurrencia en la web.");
                }
            }

            xhr.open("POST","http://localhost:8080/hackaton/cars/");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.send(JSON.stringify(e.detail.car));
        }
        
        console.log("car-main.js - carFormStore - fin");
        
        this.showCarForm = false;
    }
}

customElements.define('car-main', CarMain)