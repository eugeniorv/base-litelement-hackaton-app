import { LitElement, html } from "lit-element";
import '../market-header/market-header.js';
import '../market-sidebar/market-sidebar.js';
import '../clients-main/clients-main.js';
import '../car-main/car-main.js';
import '../purchase-main/purchase-main.js';
import '../market-footer/market-footer.js';

class MarketApp extends LitElement{

    static get properties(){
      return {
        cars : {type: Array},
        purchases : {type: Array},
        clients : {type: Array},
        showCars: {type: Boolean},
        showClients: {type: Boolean},
        showPurchases: {type: Boolean}
      };
    }

    constructor(){
      super();

      this.cars = [];
      this.purchases = [];
      this.clients = [];

      this.showCars = true; //Lógica de negocio, vendemos coches
      this.showClients = false;
      this.showPurchases = false;
    }

    render(){
      return html`
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <market-header></market-header>
      <div class="row">
        <market-sidebar class="col-2" @new-car="${this.newCar}" @new-client="${this.newClient}" @list-purchases="${this.listPurchases}" @list-cars="${this.listCars}" @list-clients="${this.listClients}"></market-sidebar>
        <clients-main id="clients-main-id" class="col-10 d-none" @updated-clients="${this.updatedClients}"></clients-main>
        <car-main id="car-main-id" class="col-10" @updated-cars="${this.updatedCars}"></car-main>
        <purchase-main id="purchase-main-id" class="col-10 d-none" @updated-purchases="${this.updatedPurchases}"></purchase-main>
      </div>
      <market-footer></market-footer>
    `;
    }

  updated(changedProperties) {
      console.log("market-app.js - updated(changedProperties)");

  }

  newCar(e) {
      console.log("market-app.js - newCar");
      this.shadowRoot.querySelector("car-main").showCarForm = true;

      console.log("market-app.js - showCarForm " + this.shadowRoot.querySelector("car-main").showCarForm);
  }
 
  newClient(e) {
    console.log("market-app.js - newClient");
    console.log("market-app.js - Se va a crear un nuevo cliente");
    this.shadowRoot.querySelector("clients-main").showClientForm = true;
  }
  
  //Prueba navegación
  listCars(e) {
    console.log("market-app.js - listCars");
    this.shadowRoot.querySelector("car-main").showCarForm = false;

    this.shadowRoot.getElementById("car-main-id").classList.remove("d-none");
    this.shadowRoot.getElementById("clients-main-id").classList.add("d-none");
    this.shadowRoot.getElementById("purchase-main-id").classList.add("d-none");

    console.log("market-app.js - listCars " + this.shadowRoot.querySelector("car-main").showCarForm);
  }

  listClients(e) {
    console.log("market-app.js - listClients");
    
    this.shadowRoot.querySelector("car-main").showClientForm = false;
    console.log("market-app.js - listClients " + this.shadowRoot.querySelector("car-main").showClientForm);

    this.shadowRoot.getElementById("clients-main-id").classList.remove("d-none");
    this.shadowRoot.getElementById("car-main-id").classList.add("d-none");
    this.shadowRoot.getElementById("purchase-main-id").classList.add("d-none");
  }

  listPurchases(e) {
    console.log("market-app.js - listPurchases");
    this.shadowRoot.querySelector("purchase-main").showPurchaseForm = false;
    console.log("market-app.js - listPurchases " + this.shadowRoot.querySelector("purchase-main").showPurchaseForm);

    this.shadowRoot.getElementById("purchase-main-id").classList.remove("d-none");
    this.shadowRoot.getElementById("car-main-id").classList.add("d-none");
    this.shadowRoot.getElementById("clients-main-id").classList.add("d-none");
  }

  updatedClients(e) {
    console.log("market-app.js - updatedClients");

    this.clients = e.detail.clients;
  }

  updatedCars(e) {
      console.log("market-app.js - updatedCars");

      this.cars = e.detail.cars;
  }

  updatedPurchases(e) {
    console.log("market-app.js - updatedPurchases");

    this.purchases = e.detail.purchases;
  }

}

customElements.define('market-app', MarketApp);
