import { LitElement, html } from "lit-element";

class MarketSidebar extends LitElement{

  updated(changedProperties){
    console.log("updated en market-sidebar");
  }

  render(){
    return html`
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
      <aside>
        <section class="p-3 mb-2 bg-light text-dark">
        <div class="mt-5">
          <button @click="${this.listClients}" class="w-70 btn btn-dark" style="font-size: 20px"><strong>Clientes</strong></button>&nbsp
          <button @click="${this.newClient}" class="w-20 btn btn-secondary" style="font-size: 20px"><strong>+</strong></button>
        </div>
        <div class="mt-5">
          <button @click="${this.listCars}" class="w-70 btn btn-dark" style="font-size: 20px"><strong>Coches</strong></button>&nbsp
          <button @click="${this.newCar}" class="w-20 btn btn-secondary" style="font-size: 20px"><strong>+</strong></button>
        </div>
        <div class="mt-5">
          <button @click="${this.listPurchases}" class="w-70 btn btn-dark" style="font-size: 20px"><strong>Purchases</strong></button>
        </div>
        </section>
      </aside>
    `;
  }

  listClients(e){
    console.log("listClients");

    this.dispatchEvent(new CustomEvent("list-clients", {}));
  }

  listCars(e){
    console.log("market-sidebar.js - listCars");

    this.dispatchEvent(new CustomEvent("list-cars", {}));
  }

  listPurchases(e){
    console.log("market-sidebar.js - listPurchases");

    this.dispatchEvent(new CustomEvent("list-purchases", {}));
  }
  
  newCar(e) {
    console.log("market-sidebar.js - newCar");
    console.log("market-sidebar.js - Se va a crear un nuevo coche");

  this.dispatchEvent(new CustomEvent("new-car", {}));
  }

  newClient(e) {
    console.log("market-sidebar.js - newClient");
    console.log("market-sidebar.js - Se va a crear un nuevo cliente");

  this.dispatchEvent(new CustomEvent("new-client", {}));
  }
}

customElements.define('market-sidebar', MarketSidebar);
