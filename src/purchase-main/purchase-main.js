import { LitElement, html} from 'lit-element';

import '../purchase-dm/purchase-dm.js';
import '../purchase-ficha-listado/purchase-ficha-listado.js';
//import '../purchase-form/purchase-form.js';

class PurchaseMain extends LitElement {

    static get properties() {
        return {
            purchases : {type: Array},
            showPurchaseForm: {type: Boolean}
        };
    }

    constructor() {
        super();

        this.purchases = [];
        this.showPurchaseForm = false;
    }

    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <section class="p-3 mb-2 bg-light text-dark">
            <h2 class="text-center">Listado de Compras</h2>
            <div id="purchaseList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${ this.purchases.map(
                    purchase => html`<purchase-ficha-listado 
                                        id="${purchase.id}" 
                                        userId="${purchase.userId}" 
                                        amount="${purchase.amount}" 
                                        purchaseItems="${purchase.purchaseItems}" 
                                        @delete-purchase="${this.deletePurchase}" 
                                        @info-purchase="${this.infoPurchase}"
                                    >
                                </purchase-ficha-listado>`
                )}
                </div>
            </div>

        </section>
        <purchase-dm
            @purchases-data-updated="${this.purchasesDataUpdated}">
        </purchase-dm>
        `;
    }

    updated(changedProperties) {
        console.log("purchase-main.js - updated(changedProperties)");
        
        if(changedProperties.has("showPurchaseForm")) {
            console.log("purchase-main.js - Ha cambiado el valor de la propiedad showPurchaseForm");

            if(this.showPurchaseForm === true){
                this.showPurchaseFormData();
            } else {
                this.showPurchaseList();
            }
        }

        if(changedProperties.has("purchases")) {
            console.log("purchase-main.js - He cambiado el valor de la propiedad purchases");

            //TO DO Throw event
            this.dispatchEvent(
                new CustomEvent(
                    "updated-purchases",
                    {
                        detail : {
                            purchases : this.purchases
                        }
                    }
                )
            )

        }

    }

    purchasesDataUpdated(e) {
        console.log("purchase-main.js - purchasesDataUpdated");
    
        this.purchases = e.detail.purchases;
    }

    deletePurchase(e) {
        console.log("purchase-main.js - deletePurchase");
        console.log("purchase-main.js - Se va a borrar el purchase id " + e.detail.id);

        // REFACTORIZACIÓN API
        // SI SE COMENTA HAY QUE REpurchaseGAR LA PÁGINA!
        this.purchases = this.purchases.filter(
                purchase => purchase.id != e.detail.id
        );

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 200 - OK - Petición realizada correctamente");

            }else if (xhr.status === 400){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 400 - BAD REQUEST - Esta respuesta significa que el servidor no pudo interpretar la solicitud dada una sintaxis inválida.");

            }else if (xhr.status === 404){
                //Ejemplo de async -> este log sale lo último en la carga de la página
                console.log("STATUS 404 - NOT FOUND - El servidor no pudo encontrar el contenido solicitado. Este código de respuesta es uno de los más famosos dada su alta ocurrencia en la web.");
            }
        }

        xhr.open("DELETE","http://localhost:8080/hackaton/purchases/"+e.detail.id);
        xhr.send();
    }

    showPurchaseList() {
        console.log("purchase-main.js - showPurchaseList");
        console.log("purchase-main.js - Mostrando el listado de coches");

        this.shadowRoot.getElementById("purchaseList").classList.remove("d-none");
        //this.shadowRoot.getElementById("purchaseForm").classList.add("d-none");
    }

    showPurchaseFormData() {
        console.log("purchase-main.js - showPurchaseFormData");
        console.log("purchase-main.js - Mostrando formulario de coche");

        //this.shadowRoot.getElementById("purchaseForm").classList.remove("d-none");
        this.shadowRoot.getElementById("purchaseList").classList.add("d-none");
    }

    infoPurchase(e) {
        console.log("purchase-main.js - infoPurchase");
        console.log("purchase-main.js - Se ha pedido más información del coche " + e.detail.id);

        let choosenPurchase = this.purchases.filter(
            purchase => purchase.id === e.detail.id
        );
        console.log(this.purchases);
        console.log(choosenPurchase);
        console.log(choosenPurchase[0].id);
  

        let purchase = {};
        purchase.id = choosenPurchase[0].id;
        purchase.userid = choosenPurchase[0].userid;
        purchase.amount = choosenPurchase[0].amount;
        purchase.purchaseItems = choosenPurchase[0].purchaseItems;

        //this.shadowRoot.querySelector
        this.shadowRoot.getElementById("purchaseForm").purchase = purchase;
        this.shadowRoot.getElementById("purchaseForm").editingPurchase = true;
        this.showPurchaseForm = true;
    }

    purchaseFormClose() {
        console.log("purchase-main.js - purchaseFormClose");
        console.log("purchase-main.js - Se ha cerrado el formulario de purchase");

        this.showPurchaseForm = false;
    }

    purchaseFormStore(e) {
        console.log("purchase-main.js - purchaseFormStore - inicio");
        console.log(JSON.stringify(e.detail.purchase));
        
        if(e.detail.editingPurchase === true) {
            console.log("purchase-main.js - Se va a actualizar el coche id " + e.detail.purchase.id);
            console.log("purchase-main.js - UPDATE purchase "+e.detail.purchase);
            
            // UPDATE
            this.purchases = this.purchases.map(
                purchase => purchase.id === e.detail.purchase.id 
                    ? purchase = e.detail.purchase : purchase
            );

            console.log("purchase-main.js -purchase "+e.detail.purchase);

            let xhr = new XMLHttpRequest();

            xhr.onload = () => {
                if (xhr.status === 200){
                    //Ejemplo de async -> este log sale lo último en la carga de la página
                    console.log("STATUS 200 - OK - Petición realizada correctamente");

                }else if (xhr.status === 400){
                    //Ejemplo de async -> este log sale lo último en la carga de la página
                    console.log("STATUS 400 - BAD REQUEST - Esta respuesta significa que el servidor no pudo interpretar la solicitud dada una sintaxis inválida.");

                }else if (xhr.status === 404){
                    //Ejemplo de async -> este log sale lo último en la carga de la página
                    console.log("STATUS 404 - NOT FOUND - El servidor no pudo encontrar el contenido solicitado. Este código de respuesta es uno de los más famosos dada su alta ocurrencia en la web.");

                }else if (xhr.status === 405){
                    //Ejemplo de async -> este log sale lo último en la carga de la página
                    console.log("STATUS 405 - NOT ALLOWED");
                }
            }

            xhr.open("PUT","http://localhost:8080/hackaton/purchases/"+e.detail.purchase.id);
            xhr.send(e.detail.purchase);

        } else {
            console.log("purchase-main.js - Se va a almacenar el coche "+e.detail.purchase.id);
            console.log("purchase-main.js - ADD purchase "+e.detail.purchase);

            // ADD
            this.purchases = [...this.purchases, e.detail.purchase]; //JS Spread Syntax(Python sequence)
            //this.purchases = [this.purchases, e.detail.purchase]; //Spread syntax: array cuyo 1er elemento es el array original

            console.log("purchase-main.js - this.purchases: "+this.purchases);

            let xhr = new XMLHttpRequest();

            xhr.onload = () => {
                if (xhr.status === 200){
                    //Ejemplo de async -> este log sale lo último en la carga de la página
                    console.log("STATUS 200 - OK - Petición realizada correctamente");

                }else if (xhr.status === 400){
                    //Ejemplo de async -> este log sale lo último en la carga de la página
                    console.log("STATUS 400 - BAD REQUEST - Esta respuesta significa que el servidor no pudo interpretar la solicitud dada una sintaxis inválida.");

                }else if (xhr.status === 404){
                    //Ejemplo de async -> este log sale lo último en la carga de la página
                    console.log("STATUS 404 - NOT FOUND - El servidor no pudo encontrar el contenido solicitado. Este código de respuesta es uno de los más famosos dada su alta ocurrencia en la web.");
                }
            }

            xhr.open("POST","http://localhost:8080/hackaton/purchases/");
            xhr.send(e.detail.purchase);
        }
        
        console.log("purchase-main.js - purchaseFormStore - fin");
        
        this.showPurchaseForm = false;
    }
}

customElements.define('purchase-main', PurchaseMain)